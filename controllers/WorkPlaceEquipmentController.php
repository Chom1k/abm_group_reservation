<?php

namespace app\controllers;

use Yii;
use yii\widgets\ActiveForm;
use yii\web\Response;
use app\models\WorkPlaceEquipment;
use app\models\WorkPlaceEquipmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WorkPlaceEquipmentController implements the CRUD actions for WorkPlaceEquipment model.
 */
class WorkPlaceEquipmentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WorkPlaceEquipment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkPlaceEquipmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new WorkPlaceEquipment(['scenario' => WorkPlaceEquipment::SCENARIO_CREATE]);

        if ( ! Yii::$app->request->isAjax) return;
        
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->save();
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    public function actionValidate(bool $isNewRecord = false)
    {
        $scenario = $isNewRecord ? WorkPlaceEquipment::SCENARIO_CREATE : WorkPlaceEquipment::SCENARIO_UPDATE; 
        $model = new WorkPlaceEquipment(['scenario' => $scenario]);

        if ( ! Yii::$app->request->isAjax) return;
        
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * Updates an existing WorkPlaceEquipment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = WorkPlaceEquipment::SCENARIO_UPDATE; 

        if ( ! Yii::$app->request->isAjax) return;
        
        if($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $model->save();
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WorkPlaceEquipment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        
        // return $this->redirect(['index']);
    }

    /**
     * Finds the WorkPlaceEquipment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorkPlaceEquipment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WorkPlaceEquipment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
