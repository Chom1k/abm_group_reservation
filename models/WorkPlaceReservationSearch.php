<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WorkPlaceReservation;

/**
 * WorkPlaceReservationSearch represents the model behind the search form of `app\models\WorkPlaceReservation`.
 */
class WorkPlaceReservationSearch extends WorkPlaceReservation
{
    public $workPlaceName;
    public $employeeName;
    public $master_search;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'work_place_id', 'employee_id', 'created_at', 'updated_at'], 'integer'],
            [['reservation_date_start', 'reservation_date_end', 'master_search', 'workPlaceName', 'employeeName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkPlaceReservation::find();

        // add conditions that should always apply here
        $query->joinWith(['workPlace', 'employee']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'work_place_id' => $this->work_place_id,
            'employee_id' => $this->employee_id,
            'reservation_date_start' => $this->reservation_date_start,
            'reservation_date_end' => $this->reservation_date_end,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
