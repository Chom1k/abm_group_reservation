<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipment".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $model
 * @property float|null $worth
 * @property string|null $description
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type', 'model'], 'required'],
            [['worth'], 'number'],
            [['name', 'type', 'model', 'description'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID', 
            'name' => 'Nazwa',
            'type' => 'Typ',
            'model' => 'Model',
            'worth' => 'Wartość',
            'description' => 'Opis',
        ];
    }

    public function getFullDescription()
    {
        return "{$this->type} - {$this->name} {$this->model}";
    }
}
