<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_place".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 */
class WorkPlace extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_place';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
            'description' => 'Opis',
        ];
    }

        /**
     * Gets query for [[WorkPlaceEquipments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkPlaceEquipments()
    {
        return $this->hasMany(WorkPlaceEquipment::className(), ['work_place_id' => 'id']);
    }

    /**
     * Gets query for [[WorkPlaceReservations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkPlaceReservations()
    {
        return $this->hasMany(WorkPlaceReservation::className(), ['work_place_id' => 'id']);
    }
}
