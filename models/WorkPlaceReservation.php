<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "work_place_reservation".
 *
 * @property int $id
 * @property int $work_place_id
 * @property int $employee_id
 * @property string $reservation_date_start
 * @property string $reservation_date_end
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Employee $employee
 * @property WorkPlace $workPlace
 */
class WorkPlaceReservation extends \yii\db\ActiveRecord
{
    // public function beforeSave($insert)
    // {
    //     if( ! parent::beforeSave($insert)) {
    //         return false;
    //     }

    //     $this->reservation_date_start = strtotime($this->reservation_date_start);
    //     $this->reservation_date_end = strtotime($this->reservation_date_end);
        
    //     return true;
    // }
    
    public function behaviors()
    {
        return [
            'class' => TimestampBehavior::className(),
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_place_reservation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['work_place_id', 'employee_id', 'reservation_date_start', 'reservation_date_end'], 'required'],
            [['work_place_id', 'employee_id', 'created_at', 'updated_at'], 'integer'],
            [['reservation_date_start', 'reservation_date_end'], 'safe'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['work_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkPlace::className(), 'targetAttribute' => ['work_place_id' => 'id']],
            ['reservation_date_end', 'validateReservationDate'],
            ['reservation_date_end', 'validateReservationEndBeforeReservationStart'],
        ];
    }

    public function validateReservationEndBeforeReservationStart($attribute, $params)
    {
        if($this->reservation_date_start >= $this->reservation_date_end) {
            $this->addError('reservation_date_end', 'Koniec rezerwacji nie może upłynąć przed jej rozpoczęciem!');
        }
    }

    public function validateReservationDate($attribute, $params)
    {
        $startTime = $this->reservation_date_start;
        $endTime = $this->reservation_date_end;

        $expression = new Expression("reservation_date_start < '{$startTime}' AND reservation_date_end > '{$startTime}' AND work_place_id = {$this->work_place_id}");
        $expression2 = new Expression("reservation_date_start < '{$endTime}' AND reservation_date_end > '{$endTime}' AND work_place_id = {$this->work_place_id}");
        $expression3 = new Expression("reservation_date_start > '{$startTime}' AND reservation_date_end < '{$endTime}' AND work_place_id = {$this->work_place_id}");
        $result = (new Query())
                        ->select('*')
                        ->from('work_place_reservation')
                        ->where($expression)
                        ->orWhere($expression2)
                        ->orWhere($expression3)
                        ->all();

        $count = count($result);

        if($count > 0) {
            $this->addError('reservation_date_start', "Istnieje już rezerwacja w tym przedziale czasowym");
            $this->addError('reservation_date_end', "Istnieje już rezerwacja w tym przedziale czasowym"); 
        }
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'work_place_id' => 'ID Miejsca pracy',
            'employee_id' => 'ID Pracownika',
            'reservation_date_start' => 'Start rezerwacji',
            'reservation_date_end' => 'Koniec rezerwacji',
            'created_at' => 'Utworzono',
            'updated_at' => 'Zaktualizowano',
        ];
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[WorkPlace]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkPlace()
    {
        return $this->hasOne(WorkPlace::className(), ['id' => 'work_place_id']);
    }
}
