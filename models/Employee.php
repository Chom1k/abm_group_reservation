<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $email
 * @property string|null $description
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone_number', 'email'], 'required'],
            [['first_name', 'last_name', 'phone_number', 'email', 'description'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['phone_number'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Imię',
            'last_name' => 'Nazwisko',
            'phone_number' => 'Numer telefonu',
            'email' => 'Email',
            'description' => 'Opis',
        ];
    }

    public function getFullName()
    {
        return "{$this->last_name} {$this->first_name}";
    }
}
