<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WorkPlaceEquipment;

/**
 * WorkPlaceEquipmentSearch represents the model behind the search form of `app\models\WorkPlaceEquipment`.
 */
class WorkPlaceEquipmentSearch extends WorkPlaceEquipment
{
    public $workPlaceName;
    public $equipmentFullDescription;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'equipment_id'], 'integer'],
            [['work_place_id', 'workPlaceName', 'equipmentFullDescription'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkPlaceEquipment::find();

        $query->joinWith(['workPlace', 'equipment']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'work_place_id' => $this->work_place_id,
            // 'equipment_id' => $this->equipment_id,
            'work_place.name' => $this->workPlaceName,
            'equipment.fullDescription' => $this->equipmentFullDescription,
        ]);

        return $dataProvider;
    }
}
