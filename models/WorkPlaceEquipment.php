<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work_place_equipment".
 *
 * @property int $id
 * @property int $work_place_id
 * @property int $equipment_id
 *
 * @property Equipment $equipment
 * @property WorkPlace $workPlace
 */
class WorkPlaceEquipment extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    
    /** 
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_place_equipment';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['work_place_id', 'equipment_id'];
        $scenarios[self::SCENARIO_UPDATE] = ['work_place_id'];
        return $scenarios;
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['work_place_id', 'equipment_id'], 'required'],
            [['work_place_id', 'equipment_id'], 'integer'],
            [['equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Equipment::className(), 'targetAttribute' => ['equipment_id' => 'id']],
            [['work_place_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkPlace::className(), 'targetAttribute' => ['work_place_id' => 'id']],
            ['equipment_id', 'unique', 'message' => 'Podane wyposażenie jest już połączone z innym miejscem pracy', 'on' => self::SCENARIO_CREATE],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'work_place_id' => 'Work Place ID',
            'equipment_id' => 'Equipment ID',
            'workPlaceName' => 'Miejsce pracy',
            'equipmentFullDescription' => 'Wyposażenie',
        ];
    }

    public function extraFields()
    {
        return ['equipmentFullDescription'];
    }

    /**
     * Gets query for [[Equipment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['id' => 'equipment_id']);
    }

    /**
     * Gets query for [[WorkPlace]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkPlace()
    {
        return $this->hasOne(WorkPlace::className(), ['id' => 'work_place_id']);
    }

    public function getEquipmentFullDescription()
    {
        return $this->equipment->fullDescription;
    }
}
