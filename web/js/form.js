$(document).ready(function() {
    var body = $('body');
    var activeForm = $('.active-form');
    var modal = $('.modal');

    body.on('beforeSubmit', '.active-form', function(event) {
        var form = $(this);

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
        })
        .done(function(response) {
            modal.modal('hide');
        });

        return false;
    });
});