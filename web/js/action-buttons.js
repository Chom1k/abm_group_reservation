$(document).ready(function() {
    const mainContainer = $('.main-container');
    // var content = $('.index-container');
    var modal = $('.modal');
    var modalBody = $('.modal-body');
    
    mainContainer.on('click', '.create-button', function() {
        let controller = $('.index-container').attr('data-controller');
        modalBody.load(`/${controller}/create`);
        modal.modal('toggle');
    });
    
    mainContainer.on('click', '.update-button', function() {
        let controller = $('.index-container').attr('data-controller');
        let id = $(this).attr('data-record-id');
        modalBody.load(`/${controller}/update?id=`+id);
        modal.modal('toggle');
    });
    
    mainContainer.on('click', '.delete-button', function() {
        let controller = $('.index-container').attr('data-controller');
        let id = $(this).attr('data-record-id');

        $.ajax({
            url: `/${controller}/delete?id=`+id,
            type: 'POST',
            data: { id: id},
            beforeSend: function() {
                let confirmation = confirm('Czy jesteś pewny czy chcesz usunąć ten rekord?')
                if( ! confirmation) return false;
            }
        })
        .done(function() {
            $('.index-container').load(`/${controller}/index`);
        });
    });

    modal.on('hidden.bs.modal', function(event) {
        let controller = $('.index-container').attr('data-controller');
        $('.index-container').load(`/${controller}/index`, function() {
            alert(`MODAL HIDDEN LOADED ${controller}`);
        });
    });
});