$(document).ready(function() {
    var content = $('.content');
    var reservationButton = $('.reservation-button');
    var mapWorkplaceEquipmentButton = $('.map-work-place-equipment-button');
    var employeeButton = $('.employee-button');
    var workPlaceButton = $('.work-place-button');
    var equipmentButton = $('.equipment-button');

    reservationButton.on('click', function() {
        content.load('/work-place-reservation/index');
    });

    mapWorkplaceEquipmentButton.on('click', function() {
        content.load('/work-place-equipment/index');
    });

    employeeButton.on('click', function() {
        content.load('/employee/index');
    });

    workPlaceButton.on('click', function() {
        content.load('/work-place/index');
    });

    equipmentButton.on('click', function() {
        content.load('/equipment/index');
    });

});