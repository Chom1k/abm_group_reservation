<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%equipment}}`.
 */
class m200323_225604_create_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%equipment}}', [
            'id' => $this->primaryKey(), 
            'name' => $this->string()->notNull()->unique(),
            'type' => $this->string()->notNull(),
            'model' => $this->string()->notNull(),
            'worth' => $this->float(),
            'description' => $this->string(),
        ]);

        $this->createIndex('idx-equipment-name', 'equipment', 'name');
        $this->createIndex('idx-equipment-type', 'equipment', 'type');
        $this->createIndex('idx-equipment-model', 'equipment', 'model');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%equipment}}');
    }
}
