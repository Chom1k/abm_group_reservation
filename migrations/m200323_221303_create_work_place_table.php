<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_place}}`.
 */
class m200323_221303_create_work_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%work_place}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'description' => $this->string(),
        ]);

        $this->createIndex('idx-work_place-name', 'work_place', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%work_place}}');
    }
}
