<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee}}`.
 */
class m200323_225013_create_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'phone_number' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'description' => $this->string(),
        ]);

        $this->createIndex('idx-employee-first_name', 'employee', 'first_name');
        $this->createIndex('idx-employee-last_name', 'employee', 'last_name');
        $this->createIndex('idx-employee-email', 'employee', 'email');
        $this->createIndex('idx-employee-phone_number', 'employee', 'phone_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee}}');
    }
}
