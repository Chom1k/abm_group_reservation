<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_place_equipment}}`.
 */
class m200323_230156_create_work_place_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%work_place_equipment}}', [
            'id' => $this->primaryKey(),
            'work_place_id' => $this->integer()->notNull(),
            'equipment_id' => $this->integer()->notNull(),
            'FOREIGN KEY(work_place_id) REFERENCES work_place(id)',
            'FOREIGN KEY(equipment_id) REFERENCES equipment(id)',
        ]);

        $this->createIndex('idx-work_place_equipment-work_place_id', 'work_place_equipment', 'work_place_id');
        $this->createIndex('idx-work_place_equipment-equipment_id', 'work_place_equipment', 'equipment_id');

        // $this->addForeignKey('fk-wpe-work_place_id-work_place-id', 'work_place_equipment', 'work_place_id', 'work_place', 'id');
        // $this->addForeignKey('fk-wpe-equipment_id-equipment-id', 'work_place_equipment', 'equipment_id', 'equipment', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // $this->dropForeignKey('fk-wpe-work_place_id-work_place-id', 'work_place_equipment');
        // $this->dropForeignKey('fk-wpe-equipment_id-equipment-id', 'work_place_equipment');
        
        $this->dropTable('{{%work_place_equipment}}');
    }
}
