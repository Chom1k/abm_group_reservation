<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_place_reservation}}`.
 */
class m200323_231403_create_work_place_reservation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%work_place_reservation}}', [
            'id' => $this->primaryKey(),
            'work_place_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'reservation_date_start' => $this->datetime()->notNull(),
            'reservation_date_end' => $this->datetime()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY(work_place_id) REFERENCES work_place(id)',
            'FOREIGN KEY(employee_id) REFERENCES employee(id)',
        ]);

        $this->createIndex('idx-work_place_reservation-work_place_id', 'work_place_reservation', 'work_place_id');
        $this->createIndex('idx-work_place_reservation-employee_id', 'work_place_reservation', 'employee_id');
        $this->createIndex('idx-work_place_reservation-reservation_date_start', 'work_place_reservation', 'reservation_date_start');
        $this->createIndex('idx-work_place_reservation-reservation_date_end', 'work_place_reservation', 'reservation_date_end');

        // $this->addForeignKey('fk-wpr-work_place_id-work_place-id', 'work_place_reservation', 'work_place_id', 'work_place', 'id');
        // $this->addForeignKey('fk-wpr-employee_id-employee-id', 'work_place_reservation', 'employee_', 'work_place', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // $this->dropForeignKey('fk-wpr-work_place_id-work_place-id', 'work_place_reservation');
        // $this->dropForeignKey('fk-wpr-employee_id-employee-id', 'work_place_reservation');
        
        $this->dropTable('{{%work_place_reservation}}');
    }
}
