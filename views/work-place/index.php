<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\ActionButtonsAsset;

ActionButtonsAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkPlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Miejsca pracy';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-place-index index-container" data-controller="work-place">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="before-table-container">
        <?= Html::button('Dodaj miejsce pracy', ['class' => 'btn btn-success create-button', 'style' => 'width: 200px;']) ?>
        <?php //echo $this->render('_master-search', ['model' => $searchModel]); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'description',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model, $key) {
                        return Html::button('Aktualizuj', ['class' => 'btn btn-primary btn-sm update-button', 'data-record-id' => $key]);
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::button('Usuń', ['class' => 'btn btn-danger btn-sm delete-button', 'data-record-id' => $key]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>