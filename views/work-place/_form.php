<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\FormAsset;

FormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\WorkPlace */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-place-form">

<?php $form = ActiveForm::begin([
        'id' => 'work-place-activeform',
        'enableAjaxValidation' => true,
        'validationUrl' => '/work-place/validate',
        'options' => ['class' => 'active-form'],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Dodaj', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>