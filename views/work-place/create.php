<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkPlace */

$this->title = 'Utwórz nowe miejsce pracy';
$this->params['breadcrumbs'][] = ['label' => 'Miejsce Pracy', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-place-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
