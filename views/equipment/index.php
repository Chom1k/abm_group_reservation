<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\ActionButtonsAsset;

ActionButtonsAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\EquipmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wyposażenie';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipment-index index-container" data-controller="equipment">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="before-table-container">
        <?= Html::button('Dodaj wyposażenie', ['class' => 'btn btn-success create-button', 'style' => 'width: 200px;']) ?>
        <?php //echo $this->render('_master-search', ['model' => $searchModel]); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'type',
            'model',
            'worth',
            'description',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model, $key) {
                        return Html::button('Aktualizuj', ['class' => 'btn btn-primary btn-sm update-button', 'data-record-id' => $key]);
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::button('Usuń', ['class' => 'btn btn-danger btn-sm delete-button', 'data-record-id' => $key]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
