<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\FormAsset;

FormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">

<?php $form = ActiveForm::begin([
        'id' => 'employee-activeform',
        'enableAjaxValidation' => true,
        'validationUrl' => '/employee/validate',
        'options' => ['class' => 'active-form'],
    ]); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Dodaj', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>