<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\ActionButtonsAsset;

ActionButtonsAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista Pracowników';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index index-container" data-controller="employee">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Dodaj pracownika', ['class' => 'btn btn-success create-button']) ?> 
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'first_name',
            'last_name',
            'phone_number',
            'email:email',
            'description',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model, $key) {
                        return Html::button('Aktualizuj', ['class' => 'btn btn-primary btn-sm update-button', 'data-record-id' => $key]);
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::button('Usuń', ['class' => 'btn btn-danger btn-sm delete-button', 'data-record-id' => $key]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
