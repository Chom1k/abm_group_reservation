<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'System rezerwacji';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Witamy w systemie rezerwacji!</h1>

        <p class="lead">Tutaj w sposób klarowny zarezerwujesz sobie odpowiednie miejsce pracy.</p>

        <?= Html::button('Rezerwuj', ['class' => 'btn btn-lg btn-danger reservation-button']); ?>
    </div>


</div>

