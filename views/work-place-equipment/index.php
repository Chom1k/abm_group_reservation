<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\ActionButtonsAsset;

ActionButtonsAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkPlaceEquipmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista wyposażenia miejsc pracy';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-place-equipment-index index-container" data-controller="work-place-equipment">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Połącz wyposażenie', ['class' => 'btn btn-success create-button']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'workPlaceName',
                'value' => function($model, $key, $index) {
                    return $model->workPlace->name ?? '-';
                }
            ],
            [
                'attribute' => 'equipmentFullDescription',
                'value' => function($model, $key, $index) {
                    return $model->equipment->fullDescription ?? '-';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model, $key) {
                        return Html::button('Aktualizuj', ['class' => 'btn btn-primary btn-sm update-button', 'data-record-id' => $key]);
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::button('Usuń', ['class' => 'btn btn-danger btn-sm delete-button', 'data-record-id' => $key]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>