<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkPlaceEquipment */

$this->title = 'Aktualizuj wyposażenie z miejscem pracy: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lista wyposażenia miejsc pracy', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-place-equipment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
