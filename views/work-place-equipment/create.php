<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkPlaceEquipment */

$this->title = 'Połącz wyposażenie z miejscem pracy';
$this->params['breadcrumbs'][] = ['label' => 'Lista wyposażenia miejsc pracy', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-place-equipment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
