<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use app\models\WorkPlace;
use app\models\Equipment;
use app\assets\FormAsset;

FormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\WorkPlaceEquipment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-place-equipment-form">

<?php $form = ActiveForm::begin([
        'id' => 'work-place-equipment-activeform',
        'enableAjaxValidation' => true,
        'validationUrl' => '/work-place-equipment/validate?isNewRecord='.$model->isNewRecord,
        'options' => ['class' => 'active-form'],
    ]); ?>

    <?= $form->field($model, 'work_place_id')
                            ->dropDownList(ArrayHelper::map(WorkPlace::find()->asArray()->all(), 'id', 'name'))
                            ->label('Miejsce pracy');
    ?>

    <?php
        $equipmentIdInput;
        if($model->isNewRecord) 
        {
            echo $form->field($model, 'equipment_id')
                                    ->dropDownList(ArrayHelper::map(Equipment::find()->all(), 'id', 'fullDescription'))
                                    ->label('Wyposażenie');
        } 
        else
        {
            $equipment = $model->equipmentFullDescription;
            echo $form->field($model, 'equipment_id')
                                            ->textInput(['class' => 'form-control', 'readonly' => true, 'value' => $equipment])
                                            ->label('Wyposażenie');
        }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Połącz', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>