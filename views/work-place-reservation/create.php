<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkPlaceReservation */

$this->title = 'Utwórz rezerwacje';
$this->params['breadcrumbs'][] = ['label' => 'Rezerwacje', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-place-reservation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
