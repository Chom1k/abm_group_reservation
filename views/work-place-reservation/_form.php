<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\WorkPlace;
use app\models\Employee;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
use richardfan\widget\JSRegister;
use app\assets\FormAsset;

FormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\WorkPlaceReservation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-place-reservation-form">

    <?php $form = ActiveForm::begin([
        'id' => 'work-place-reservation-activeform',
        'enableAjaxValidation' => true,
        'action' => ['work-place-reservation/create'],
        'validationUrl' => '/work-place-reservation/validate',
        'options' => ['class' => 'active-form'],
    ]); ?>

    <?= $form->field($model, 'work_place_id')
                                    ->dropDownList(ArrayHelper::map(WorkPlace::find()->asArray()->all(), 'id', 'name'), [
                                        'id' => 'work-place-id-dropdown',
                                    ])
                                    ->label('Miejsce pracy'); ?>

    <?php
        echo Html::label('Wyposażenie', 'work-place-equipment-textarea', ['class' => 'control-label']);
        echo Html::textarea('work-place-equipment-textarea', '', [
            'class' => 'form-control', 'rows' => 5, 'id' => 'work-place-equipment-textarea', 'readonly' => true,
            ]);
    ?>

    <?= $form->field($model, 'employee_id')
                                    ->dropDownList(ArrayHelper::map(Employee::find()->all(), 'id', 'fullName'))
                                    ->label('Rezerwujący'); ?>

    <?= $form->field($model, 'reservation_date_start')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Start rezerwacji...'],
        'pluginOptions' => ['autoclose' => true],
    ]); ?>
    
    <?= $form->field($model, 'reservation_date_end')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Koniec rezerwacji...'],
        'pluginOptions' => ['autoclose' => true],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function() {
        var workPlaceIdDropdown = $('#work-place-id-dropdown');
        var workPlaceEquipmentTextArea = $('#work-place-equipment-textarea');

        workPlaceIdDropdown.on('change', function() {
            fetchWorkplaceEquipmentById(workPlaceIdDropdown.val());
        });
        
        fetchWorkplaceEquipmentById(workPlaceIdDropdown.val());
        
        function fetchWorkplaceEquipmentById(workPlaceId) {
            $.ajax({
                url: '/work-place/fetch-work-place-equipment-by-id',
                type: 'GET',
                data: { workPlaceId: workPlaceId },
            })
            .done(function(response) {
                let formatResponse = JSON.parse(response);
                updateWorkPlaceEquipmentTextArea(formatResponse);
            });
        }

        function updateWorkPlaceEquipmentTextArea(workPlaceEquipment)
        {
            workPlaceEquipmentTextArea.text('');
            
            for(var item of workPlaceEquipment)
            {
                workPlaceEquipmentTextArea.append(`${item.equipmentFullDescription}\n`);
            }
        }
    });
</script>
<?php JSRegister::end(); ?>