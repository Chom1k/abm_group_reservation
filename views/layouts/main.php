<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap main-container">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Html::beginTag('ul', ['class' => 'navbar-nav navbar-right nav']);
        echo Html::beginTag('li');
            echo Html::button('Rezerwuj', ['class' => 'btn btn-mid btn-primary reservation-button nav-button']);
            echo Html::button('Połącz wyposażenie', ['class' => 'btn btn-mid btn-danger map-work-place-equipment-button nav-button']);
            echo Html::button('Pracownicy', ['class' => 'btn btn-mid btn-default employee-button nav-button']);
            echo Html::button('Miejsca pracy', ['class' => 'btn btn-mid btn-default work-place-button nav-button']);
            echo Html::button('Wyposażenie', ['class' => 'btn btn-mid btn-default equipment-button nav-button']);
        echo Html::endTag('li');
    echo Html::endTag('ul');
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <div class="content">
            <?= $content ?>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Paweł Chomiczewski <?= date('Y') ?></p>
        <p class="pull-left" style="margin-left: 10px;"><?= Yii::$app->params['version']; ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>